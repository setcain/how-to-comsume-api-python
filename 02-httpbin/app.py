"""
File: app.py
Author: setcain
Email: setcain00@gmail.com
Description: Test API httpbin GET
"""

import requests


def main():
    """
    Use httpbin for tests whit GET method
    """
    url = 'http://www.httpbin.org/get'
    args = {'name': 'setcain', 'sername': 'setcain', 'password': 'admin'}

    res = requests.get(url, args)

    # .url gets the url whit new args
    res_url = res.url

    if res.status_code == 200:
        content = res.content

        file = open('httpbin.json', 'wb')
        file.write(content)
        file.close()

        print('Your petition is OK.\nThis is the url: {0}'.format(res_url))


if __name__ == "__main__":
    main()
