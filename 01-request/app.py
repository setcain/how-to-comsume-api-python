"""
File: app.py
Author: setcain
Email: setcain00@gmail.com
Description: Test request
"""

import requests


def main():
    """
    Makes a GET petition to google servers
    otput: 200
    create .html whit google content
    """
    url = 'http://www.google.com.mx'

    # request.get method response whit http status codes
    res = requests.get(url)

    # .status_code return http status code
    if res.status_code == 200:

        # .content return html for url in .get
        content = res.content

        # store html content whit
        file = open('google.html', 'wb')
        file.write(content)
        file.close()

        print('Your petition is OK')


if __name__ == "__main__":
    main()
