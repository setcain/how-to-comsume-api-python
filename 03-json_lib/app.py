"""
File: app.py
Author: setcain
Email: setcain00@gmail.com
Description: Use python lib json
"""
import requests

import json


def main():
    """
    Use httpbin for tests whit json lib
    """
    url = 'http://www.httpbin.org/get'
    args = {'name': 'setcain', 'sername': 'setcain', 'password': 'admin'}

    res = requests.get(url, args)

    # get response petition in text format
    res_json = json.loads(res.text)

    # origin is an attribute in json httpbin
    httpbin_origin = res_json['origin']

    if res.status_code == 200:
        print('Oringin attr from httpbin: {0}'.format(httpbin_origin))


if __name__ == "__main__":
    main()
